# Relief Apps Test - Front-End

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.3.

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:8001/`.

## Full stack
I have separated full stack and front-end only. Full stack version is in the branch `full-stack`.