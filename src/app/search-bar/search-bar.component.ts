import {Component, Input, OnInit} from '@angular/core';
import {VideoService} from "../video.service";
import {HistoryService} from "../history.service";
import {VideoViewComponent} from "../video-view/video-view.component";

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  URL: string;

  constructor(private video: VideoService, private history: HistoryService) { }

  ngOnInit() {
    this.video.currentURL.subscribe(videoURL => this.URL = videoURL)
  }

  updateURL() {
    this.video.changeVideoURL(this.URL);
    if(this.video.parse(this.URL)) {
      this.history.addEntry(this.URL);
    }
    this.URL = '';
  }

}
