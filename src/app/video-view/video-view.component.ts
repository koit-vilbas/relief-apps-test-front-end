import { Component, OnInit } from '@angular/core';
import {VideoService} from "../video.service";
import YouTubePlayer from 'youtube-player';
import {HistoryService} from "../history.service";

@Component({
  selector: 'app-video-view',
  templateUrl: './video-view.component.html',
  styleUrls: ['./video-view.component.scss']
})
export class VideoViewComponent implements OnInit {
  public videoURL;
  private player;
  private isLoaded = false;

  constructor(private video: VideoService, private history: HistoryService) {}

  ngOnInit() {
    this.video.currentURL.subscribe(URL => {
      this.videoURL = URL;

      // Extract ID and load new video
      let id = this.video.parse(URL);

      if(id) {
        if(!this.isLoaded) {
          this.player = YouTubePlayer('player', {
            height: '100%',
            width: '100%'
          });
          this.isLoaded = true;
        }

        this.player.loadVideoById(id);
      }
    });
  }

}
