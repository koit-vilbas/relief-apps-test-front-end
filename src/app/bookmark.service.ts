import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BookmarkService {

  private entries: Array<string> = [];

  constructor() {
    let entries = localStorage.getItem('bookmarkEntries');

    if(entries !== null) {
      this.entries = JSON.parse(entries);
    } else {
      localStorage.setItem('bookmarkEntries', JSON.stringify(this.entries))
    }
  }

  public getCount() {
    return this.entries.length;
  }

  public getEntries() {
    return this.entries;
  }

  public addEntry(entry:string) {
    this.entries.push(entry);
    localStorage.setItem('bookmarkEntries', JSON.stringify(this.entries));
  }
}
