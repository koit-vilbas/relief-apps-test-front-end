import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  private videoSource = new BehaviorSubject('');
  currentURL = this.videoSource.asObservable();

  constructor() { }

  changeVideoURL(URL: string) {
    this.videoSource.next(URL);
  }

  parse(URL: string) {
    const regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    const match = URL.match(regExp);

    return match && match[2].length == 11 ? match[2] : false;
  }
}
