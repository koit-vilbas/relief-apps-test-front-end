import { Component, OnInit } from '@angular/core';
import {BookmarkService} from "../bookmark.service";
import {VideoService} from "../video.service";

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.scss']
})
export class BookmarksComponent implements OnInit {

  constructor(public bookmarkService: BookmarkService, private videoService: VideoService) { }

  ngOnInit() {
  }

  getNoOfBookmarks() {
    return this.bookmarkService.getCount();
  }

  loadVideo(URL:string) {
    this.videoService.changeVideoURL(URL)
  }

}
