import { Component, OnInit } from '@angular/core';
import {HistoryService} from "../history.service";
import {VideoService} from "../video.service";

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  constructor(public service: HistoryService, private video: VideoService) { }

  ngOnInit() {
  }

  loadVideo(URL:string) {
    this.video.changeVideoURL(URL)
  }

}
