import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {
  private entries:Array<string> = [];

  constructor() {
    let entries = localStorage.getItem('historyEntries');

    if(entries !== null) {
      this.entries = JSON.parse(entries);
    } else {
      localStorage.setItem('historyEntries', JSON.stringify(this.entries))
    }
  }

  public getEntries() {
    return this.entries;
  }

  public addEntry(entry:string) {
    this.entries.unshift(entry);
    localStorage.setItem('historyEntries', JSON.stringify(this.entries));
  }
}
