import { Component, OnInit } from '@angular/core';
import {BookmarkService} from "../bookmark.service";
import {VideoService} from "../video.service";

@Component({
  selector: 'app-add-to-bookmarks',
  templateUrl: './add-to-bookmarks.component.html',
  styleUrls: ['./add-to-bookmarks.component.scss']
})
export class AddToBookmarksComponent implements OnInit {
  constructor(public bookmarkService: BookmarkService, public videoService: VideoService) { }

  currentURL:string;

  ngOnInit() {
    this.videoService.currentURL.subscribe(URL => this.currentURL = URL );
  }

  add() {
    if(this.videoService.parse(this.currentURL)) {
      this.bookmarkService.addEntry(this.currentURL);
    }
  }

}
